package be.janolaerts.springintegrationopdrachten;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.stream.CharacterStreamWritingMessageHandler;
import org.springframework.messaging.*;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.messaging.support.MessageBuilder;

@SpringBootApplication
public class MessageApp {

//    @Bean
//    public MessageChannel channel() {
//        return new PublishSubscribeChannel();
//    }
//
//    @Bean
//    public MessageHandler consumer() {
//        return new MessageHandler() {
//            @Override
//            public void handleMessage(Message<?> message) throws MessagingException {
//                System.out.println(message.getPayload());
//            }
//        };
//    }


    public static void main(String[] args) {

        ConfigurableApplicationContext ctx =
                SpringApplication.run(MessageApp.class, args);

        MessageHandler consumer = ctx.getBean("consumer", MessageHandler.class);

        // Lookup channel in container
        SubscribableChannel channel = ctx.getBean("channel", SubscribableChannel.class);

        // Create and send a message
        Message<String> message  = MessageBuilder
                .withPayload("Hello World")
                .setHeader("Text", "Test")
                .build();

        channel.send(message);

        // Poll for a new message
        // Todo: ask Ward: Not sure if correct (task2)
        boolean receivedMessage = channel.subscribe(consumer);
        System.out.println(message.getPayload());
        System.out.println(message.getHeaders());
    }
}