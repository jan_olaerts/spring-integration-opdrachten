package be.janolaerts.springintegrationopdrachten;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.stream.CharacterStreamReadingMessageSource;
import org.springframework.integration.stream.CharacterStreamWritingMessageHandler;
import org.springframework.messaging.*;
import org.springframework.messaging.support.MessageBuilder;

@SpringBootApplication
public class AdapterApp {

//    @Bean
//    public MessageChannel channel() {
//        return new QueueChannel();
//    }
//
//    @Bean
//    @InboundChannelAdapter(value="channel",
//                           poller=@Poller(fixedRate="${poller.fixedRate}"))
//    public MessageSource<String> producer() {
//
//        return CharacterStreamReadingMessageSource.stdin();
//    }
//
//    @Bean
//    @ServiceActivator(inputChannel="channel", poller=@Poller(fixedRate="${poller.fixedRate}"))
//    public MessageHandler consumer() {
//
//        CharacterStreamWritingMessageHandler cswmh = CharacterStreamWritingMessageHandler.stdout();
//        cswmh.setShouldAppendNewLine(true);
//        return cswmh;
//    }

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx =
                SpringApplication.run(FileAdapterApp.class, args);

        PollableChannel channel = ctx.getBean("channel", PollableChannel.class);

        // Create and send a message
        Message<String> message  = MessageBuilder
                .withPayload("Hello World")
                .setHeader("Text", "Test")
                .build();

        channel.send(message);
    }
}