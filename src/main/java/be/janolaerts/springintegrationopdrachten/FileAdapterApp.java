package be.janolaerts.springintegrationopdrachten;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.FileNameGenerator;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.integration.file.filters.AcceptAllFileListFilter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.support.MessageBuilder;

import java.io.File;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.TimeZone;

@SpringBootApplication
public class FileAdapterApp {

    @Bean
    @InboundChannelAdapter(value="channel",
            poller=@Poller(fixedRate="${poller.fixedRate}"))
    public MessageSource<File> producer() {

        FileReadingMessageSource producer =
                new FileReadingMessageSource();

        producer.setDirectory(new File("C:\\temp\\inbound"));
        producer.setFilter(new AcceptAllFileListFilter<>());
        return producer;
    }

    @Bean
    @ServiceActivator(inputChannel="channel")
    public MessageHandler consumer() {

        FileWritingMessageHandler consumer =
                new FileWritingMessageHandler(
                        new File("C:\\temp\\outbound"));

        consumer.setDeleteSourceFiles(true);
        consumer.setExpectReply(false);
        consumer.setFileNameGenerator(new FileNameGenerator() {
            @Override
            public String generateFileName(Message<?> message) {
                Long ts = message.getHeaders().getTimestamp();
                LocalDateTime ldt = LocalDateTime.ofInstant(
                        Instant.ofEpochMilli(ts), TimeZone.getDefault().toZoneId());
                return ldt.getDayOfMonth() + "_" + ldt.getMonth() + "_" + ldt.getYear()
                        + "_" + ldt.getHour() + "_" + ldt.getMinute();
            }
        });
        return consumer;
    }

    public static void main(String[] args) {

        ConfigurableApplicationContext ctx =
            SpringApplication.run(FileAdapterApp.class, args);

        SubscribableChannel channel = ctx.getBean("channel", SubscribableChannel.class);

        // Create and send a message
        Message<String> message  = MessageBuilder
                .withPayload("Hello World")
                .setHeader("Text", "Test")
                .build();

        channel.send(message);
    }
}